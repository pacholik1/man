LOOKAHEADS AND LOOKBAHINDS
==========================

- positive lookahead
`q(?=u)` matches *q* that is followed by *u*
- negative lookahead
`q(?!u)` matches *q* that is not followed by *u*

- positive lookbahind
`(?<=u)q` matches *q* that is preceded by *u*
- negative lookbahind
`(?<!u)q` matches *q* that is not preceded by *u*
