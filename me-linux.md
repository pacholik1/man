my history:
-----------
#de:
~1/2009		Ubuntu (GNOME 2)
~11/2009	Xubuntu (Xfce)
8/2010		Debian (Xfce)
~6/2014		Debian (Awesome)

#web:
IE –2004
Firefox 2004–2006
Opera 2006–2013
Firefox 2013–2018
qutebrowser 2018–

#editor:
vim ~2013–


desktops:
---------
1	pracovní (třeba build skripty)
2	web/im
3	mimopracovní (třeba awesome konfig)
4	db
5	ssh
6	2. prohlížeč / gimp / inkscape
7	workers
8	backend
9	frontend

1	eshop3 vývoj
2	web/im
3	mimopracovní
4	db
5	ssh, vpn
6	fareon tools
7	
8	eshop2
9	eshop3 terminály


workflow:
---------
         │ ranger
    gvim ├───────
         │ term
