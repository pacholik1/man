##download f-droid
    https://f-droid.org/FDroid.apk

##install
- KDE Connect
- Password Store
- OpenKeychain

##GPG key
- export
    gpg --list-keys
    gpg -a --output /tmp/gpg-secret-key.asc --export-secret-keys SOMEKEY
- send to phone using KDE Connect
- import using OpenKeychain

##git
- generate SSH key pair
- add the key to GitLab
- clone from
    git@gitlab.com:pacholik1/pass.git
