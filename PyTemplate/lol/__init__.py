import sys

from ._version import __version__, __version_info__     # noqa: F401


def main():
    pass


if __name__ == '__main__':
    main(*sys.argv[1:])
