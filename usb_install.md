ON LINUX
========
    sudo -i
    dd bs=4M if=/tmp/firmware-9.1.0-amd64-netinst.iso of=/dev/sdc status=progress && sync

ON WINDOWS
==========
[rufus](https://rufus.akeo.ie/)

Note: Be sure to select DD mode or the image will be transferred incorrectly.
