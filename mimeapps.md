cfg in ~/.config/mimeapps.list

~/.local/share/applications/rifle.desktop
    [Desktop Entry]
    Type=Application
    Name=rifle
    Comment=ranger's file opener
    Icon=utilities-terminal
    Exec=rifle %U

command:
    mime="$(xdg-mime query filetype "$1")"
    xdg-mime default rifle.desktop "$mime"

browser:
    xdg-settings set default-web-browser qutebrowser.desktop
