-- sqlite3 ~/.local/share/qutebrowser/history.sqlite < ~/man/qutebrowser-history-cleanup.sql 

DELETE FROM History WHERE (atime < 1530000000);
DELETE FROM History WHERE url LIKE '%novinky%';
DELETE FROM History WHERE url LIKE '%lidovky%';
DELETE FROM History WHERE url LIKE '%duckduckgo%';

DELETE FROM CompletionHistory WHERE (last_atime < 1530000000);
DELETE FROM CompletionHistory WHERE url LIKE '%novinky%';
DELETE FROM CompletionHistory WHERE url LIKE '%lidovky%';
DELETE FROM CompletionHistory WHERE url LIKE '%duckduckgo%';
